# Welcome to Freedesktop SDK

[Freedesktop SDK](https://freedesktop-sdk.io/) is a free, community-developed, and open-source project with a number of components that help you simplify the process of creating different software artifacts. More common use cases include building containers, Flatpak runtimes, snaps, or complete operating systems.

The Freedesktop SDK project is not an endorsement of a particular platform or selection of technologies by the Freedesktop organization.
The SDK aims to support a common interoperable platform for projects such as GNOME, KDE, and Flatpak by providing integration and validation of a shared set of dependencies.

## Documentation

Visit the [Freedesktop SDK documentation portal](https://freedesktop-sdk.gitlab.io/documentation/) to find out more about the project and its use cases:

- [Getting started](https://freedesktop-sdk.gitlab.io/documentation/getting-started/)
- [Using the SDK](https://freedesktop-sdk.gitlab.io/documentation/using-the-sdk/)
- [Contributing to the SDK](https://freedesktop-sdk.gitlab.io/documentation/contributing/)
- [Troubleshooting](https://freedesktop-sdk.gitlab.io/documentation/troubleshooting/)

## Acknowledgements

This project wouldn't be possible without the work of a few individuals and
groups, and we would like to take a moment to thank them:

- Alex Larsson, who not only gave us [Flatpak](https://flatpak.org) but also the original [Freedesktop SDK](https://github.com/flatpak/freedesktop-sdk-images) (versions 1.2 to 1.6).
- The wider Flatpak community, of which we are only a small part, and who constantly help us.
- The [BuildStream](https://buildstream.build/) community, who gave the world this amazing tool that makes building and maintaining our project so easy and fun.
- Dodji Seketeli, who wrote [libabigail](https://sourceware.org/libabigail/), which allows us to ensure we do not break apps, and tirelessly works with us on fixing any bug we encounter.
- [Codethink](https://www.codethink.co.uk/), for assigning some of their engineers' time to this project.
- [OSU Open Source Lab](https://osuosl.org/) for the x86 runners.
- [packet](https://www.packet.com/) for the aarch64 runners.
